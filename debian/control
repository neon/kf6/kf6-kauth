Source: kf6-kauth
Section: libs
Priority: optional
Maintainer: Jonathan Esk-Riddell <jr@jriddell.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               doxygen,
               graphviz,
               kf6-extra-cmake-modules,
               kf6-kcoreaddons-dev,
               kf6-kwindowsystem-dev,
               libpolkit-qt6-1-dev,
               pkgconf,
               pkg-kde-tools-neon,
               qt6-base-dev,
               qt6-tools-dev
Standards-Version: 4.6.2
Homepage: https://projects.kde.org/projects/frameworks/kauth
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kauth
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kauth.git

Package: kf6-kauth
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: ${misc:Depends}, ${shlibs:Depends}
Replaces: libkf6auth-data,
          libkf6auth-doc,
          libkf6auth6,
          libkf6authcore6,
Description: Abstraction to system policy and authentication features
 KAuth is a framework to let applications perform actions as a
 privileged user.
 .
 KAuth is part of KDE Frameworks 6.
 .
 This package contains data files.

Package: kf6-kauth-dev
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: kf6-kauth (= ${binary:Version}),
         kf6-kcoreaddons-dev,
         qt6-base-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: libkf6auth-dev,
          libkf6auth-dev-bin,
          libkf6auth-bin-dev,
Description: Abstraction to system policy and authentication features
 KAuth is a framework to let applications perform actions as a
 privileged user.
 .
 KAuth is part of KDE Frameworks 6.
 .
 This package contains development files for kauth.

Package: libkf6auth-data
Architecture: all
Depends: kf6-kauth, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6auth-doc
Architecture: all
Depends: kf6-kauth-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6auth6
Architecture: all
Depends: kf6-kauth, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6authcore6
Architecture: all
Depends: kf6-kauth, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6auth-dev
Architecture: all
Depends: kf6-kauth-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6auth-dev-bin
Architecture: all
Depends: kf6-kauth-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6auth-bin-dev
Architecture: all
Depends: kf6-kauth-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.
